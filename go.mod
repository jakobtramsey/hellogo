module gitlab.com/jakobtramsey/hellogo

go 1.22.1

replace gitlab.com/jakobtramsey/hellogo v0.0.0 => ../mystrings

require (
	gitlab.com/jakobtramsey/mystrings v0.0.0
)
